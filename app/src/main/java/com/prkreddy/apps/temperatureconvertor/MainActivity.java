package com.prkreddy.apps.temperatureconvertor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText tempInput;
    private RadioGroup tempRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tempInput = (EditText) findViewById(R.id.editText_temperature);
        tempRadioGroup = (RadioGroup)findViewById(R.id.radioGroup_temperature);
        // listener on the radio group to change the orientation of the radio buttons (within the group)
        // based on the current selection of the buttons
        tempRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (tempInput.getText().length() == 0) {
                    //TODO: how to make Toast.this work
                    // Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
                    return;
                }

                float inputValue = Float.parseFloat(tempInput.getText().toString());

                switch (checkedId) {
                    case R.id.radio_celsius:
                        tempInput.setText(String.valueOf(TemperatureConverterUtil.convertFahrenheitToCelsius(inputValue)));
                        break;
                    case R.id.radio_fahrenheit:
                        tempInput.setText(String.valueOf(TemperatureConverterUtil.convertCelsiusToFahrenheit(inputValue)));
                        break;
                }
            }
        });
    }
}
